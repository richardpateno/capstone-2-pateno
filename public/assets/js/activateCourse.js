//We use URLSearchParams to access the specific parts of query string in the URL
//d1/pages/deleteCourse.html?courseId=5fbb5b2f62d45816a4dabe67
let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");

//how do we get the token from the localStorage?
let token = localStorage.getItem("token");

//this fetch will be run automatically once you get to the page deleteCourse page.
fetch(
  `https://nameless-cliffs-31565.herokuapp.com/api/courses/activate/${courseId}`,
  {
    method: "PUT",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }
)
  .then((res) => res.json())
  .then((data) => {
    if (data) {
      alert("Course Activated.");
    } else {
      alert("Something Went Wrong.");
    }
  });
