let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");

console.log(courseId);

//get the JWT in the localStorage
let token = localStorage.getItem("token");
let adminUser = localStorage.getItem("isAdmin");

console.log(token); //token

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");
let enrolleeContainer = document.querySelector("#enrolleeContainer");

fetch(`https://nameless-cliffs-31565.herokuapp.com/api/courses/${courseId}`)
  .then((res) => res.json())
  .then((data) => {
    console.log(data);

    courseName.innerHTML = data.name;
    courseDesc.innerHTML = data.description;
    coursePrice.innerHTML = data.price;

    if (adminUser === "true") {
      if (data.enrollees.length < 1) {
        enrollContainer.innerHTML = "No Enrollees Available";
      } else {
        data.enrollees.forEach((enrollee) => {
          console.log(enrollee);

          fetch("https://nameless-cliffs-31565.herokuapp.com/api/users/")
            .then((res) => res.json())
            .then((users) => {
              console.log(users);
              users.forEach((user) => {
                if (enrollee.userId === user._id) {
                  enrolleeContainer.innerHTML += `

							<div class="card">
							<div class="card-body">
							<h5 class="card-title">${user.firstName} ${user.lastName}</h5>
							<p class="card-text text-center">${enrollee.enrolledOn}</p>
							</div>
							</div>

							`;
                }
              });
            });
        });
      }
    } else {
      enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary" >Enroll</button>`;

      //add a click event to our button
      document.querySelector("#enrollButton").addEventListener("click", () => {
        fetch("https://nameless-cliffs-31565.herokuapp.com/api/users/enroll", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            courseId: courseId,
          }),
        })
          .then((res) => res.json())
          .then((data) => {
            console.log(data);

            //redirect the user to the courses page after enrolling

            if (data === true) {
              alert("thank you for enrolling to the course");
              window.location.replace("./courses.html");
            } else {
              alert("Something Went Wrong");
            }
          });
      });
      //no need to add e.preventDefault because the click event does not have a default behavior that refreshes the page unlike submit.
    }
  });
