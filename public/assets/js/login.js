let loginForm = document.querySelector("#logInUser");

loginForm.addEventListener("submit", (e) => {
  e.preventDefault();
  let email = document.querySelector("#userEmail").value;
  let password = document.querySelector("#password").value;

  console.log(email);
  console.log(password);

  if (email == "" || password == "") {
    alert("please input your email and/or password.");
  } else {
    fetch("https://nameless-cliffs-31565.herokuapp.com/api/users/login", {
      method: "POST",
      headers: { "content-type": "application/json" },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data.accessToken) {
          localStorage.setItem("token", data.accessToken);

          fetch(
            "https://nameless-cliffs-31565.herokuapp.com/api/users/details",
            {
              headers: {
                Authorization: `Bearer ${data.accessToken}`,
              },
            }
          )
            .then((res) => res.json())
            .then((data) => {
              console.log(data);
              localStorage.setItem("id", data.id);
              localStorage.setItem("isAdmin", data.isAdmin);

              window.location.replace("./courses.html");
            });
        } else {
          alert("login failed. something went wrong");
        }
      });
  }
});
